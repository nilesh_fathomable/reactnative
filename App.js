import * as React from 'react';
import { Button, View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {Provider } from "react-redux";
import ScrollViews from './Component/ScrollViews';
import UseStateHooks from './Component/UseStateHooks';
import FlatLists from './Component/FlatLists';
import StackNav from './Component/StackNav';
import ReduxCount from './Component/ReduxCount';
import store from './store';

const MyTheme = {
  dark: true,
  colors: {
    primary: 'rgb(255, 45, 85)',
    background: 'rgb(242, 242, 242)',
    card: 'rgb(255, 255, 255)',
    text: 'rgb(28, 28, 30)',
    border: 'rgb(199, 199, 204)',
  },
};

function HomeScreen({ navigation }) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Home Screen</Text>
      <Open navigation={navigation} />
    </View>
  );
}

function Open(props) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Button title="Open" onPress={() => props.navigation.openDrawer()} />
    </View>
  );
}

function AboutScreen(props) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>About Screen</Text>
      <Open navigation={props.navigation} />
    </View>
  );
}

function DetailsScreen(props) {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>
      <Open navigation={props.navigation} />
    </View>
  );
}

const Drawer = createDrawerNavigator();

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer theme={MyTheme}>
        <Drawer.Navigator initialRouteName="Home">
          <Drawer.Screen name="Home" component={HomeScreen} />
          <Drawer.Screen name="StackNav" component={StackNav} />
          <Drawer.Screen name="Details" component={DetailsScreen} />
          <Drawer.Screen name="About" component={AboutScreen} />
          <Drawer.Screen name="ScrollView" component={ScrollViews} />
          <Drawer.Screen name="UseStateHooks" component={UseStateHooks} />
          <Drawer.Screen name="FlatLists" component={FlatLists} />
          <Drawer.Screen name="ReduxCount" component={ReduxCount} />
        </Drawer.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

export default App;