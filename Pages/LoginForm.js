
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
    Button, TextInput
} from 'react-native';

const styles = StyleSheet.create({
    input: {
        borderBottomColor: '#F5FCFF',
        backgroundColor: '#FFFFFF',
        borderRadius: 30,
        borderBottomWidth: 1,
        width: 250,
        height: 45,
        marginBottom: 20,
        padding: 10,
        flexDirection: 'row',
        alignItems: 'center'
    },
    buttonContainer: {
        height: 45,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 20,
        width: 250,
        borderRadius: 30,
    },
});

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            password: '',
        };
    }
    onLogin() {
        const { username, password } = this.state;

        console.warn(username, password);
    }
    render() {
        console.warn(this.state)
        return (
            <View>
                <TextInput
                    value={this.state.username}
                    onChangeText={(username) => this.setState({ username })}
                    placeholder={'Username'}
                    style={styles.input}
                />
                <TextInput
                    value={this.state.password}
                    onChangeText={(password) => this.setState({ password })}
                    placeholder={'Password'}
                    secureTextEntry={true}
                    style={styles.input}
                />
                <View style={styles.buttonContainer} >
                    <Button
                        title={'Login'}
                        onPress={this.onLogin.bind(this)}
                    />
                </View>
            </View>
        );
    }
}
export default LoginForm;