// import {types} from '../actions/types';
const initialState = {
  count: 0
};
export default function(state = initialState, action) {

  switch (action.type) {
    case 'ADD':
      return {
        ...state,
        count: state.count + action.payload ,
      }

    case 'MINUS':
      return {
        ...state,
        count: state.count - action.payload,
      };

    default:
      return state;
  }
}
