// import {types} from '../actions/types';
const initialState = {
  name: ""
};
export default function(state = initialState, action) {

  switch (action.type) {
    case 'ADDUSER':
      return {
        ...state,
        name: action.payload,
      };
    default:
      return state;
  }
}
