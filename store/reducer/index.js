import {combineReducers} from 'redux';
import countReducer from './countReducer';
import userReducer from './userReducer';

export default combineReducers({
  countState: countReducer,
  userState: userReducer,
});