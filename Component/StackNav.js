import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import FlatLists from './FlatLists';
import UserData from './UserData';
import UserProfile from './UserProfile';

const Stack = createStackNavigator();
export default StackNav = () => {
  return (
    <NavigationContainer independent={true}>
      <Stack.Navigator initialRouteName="Details">
        <Stack.Screen name="Home" component={FlatLists} />
        <Stack.Screen name="userData" component={UserData} />
        <Stack.Screen
          name="User Profile"
          component={UserProfile}
          options={{title: 'Profile'}}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
