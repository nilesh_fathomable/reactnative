import React from 'react';
import {View, Text, Button} from 'react-native';

export default props => {
  const {userData} = props;

  return (
    <View style={{flex: 1, flexDirection: 'row', margin: 5, padding: 5}}>
      <View style={{flex: 2}}>
        <Text>{userData.name}</Text>
      </View>
      <View style={{flex: 1}}>
        <Button
          title="Details"
          onPress={() => props.navigation.navigate('User Profile', userData)}
        />
      </View>
    </View>
  );
};