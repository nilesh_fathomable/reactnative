
import React, { Component } from 'react';
import {
    StyleSheet,
    View,
    Text,
} from 'react-native';

const FunctionalComp = () => {
    return (
        <View><Text> this is from Functional Component </Text></View>
    )
}

export default FunctionalComp;