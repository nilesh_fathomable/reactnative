import React,{useState} from 'react';
import {Button, View, Text, ScrollView} from 'react-native';
const UseStateHooks = () => {
 const [count, setcount] = useState(1)
  return (
    <View style={{flex: 1}}>
      <Text>{count}</Text>
      <View style={{flex: 1 }}>
        <Button title="increment" onPress={() => setcount(count + 1)} />
        <Button title="decrement" onPress={() => setcount(count - 1)} />
      </View>
    </View>
  );
};
export default UseStateHooks;
