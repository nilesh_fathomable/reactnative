import * as React from 'react';
import { Button, View, Text,ScrollView } from 'react-native';
const ScrollViews = (props) =>{
    return (
      <View style = {{flex:1}}>
        <Text>vertical scroll</Text>
        <ScrollView >
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>vertical scroll</Text>
          </View>
        </ScrollView>
        <Text style={{height: 200, width: 200}} >horizontal scroll</Text>
        <ScrollView horizontal={true}>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
          <View style={{height: 200, width: 200}}>
            <Text>horizontal scroll</Text>
          </View>
        </ScrollView>
      </View>
    );
}
export default ScrollViews;