import React, {Component} from 'react';
import { connect } from "react-redux";
import { Text,View,Button,StyleSheet,TextInput} from 'react-native';

class ReduxCount extends Component {
  addincr = () => {
    const {addCount} = this.props;
    addCount(1);
  };
  minsincr = () => {
    const {minusCount} = this.props;
    minusCount(1);
  };
  addUserName = (e) => {
    const {addUserName} = this.props;
    addUserName(e);
  };
  
  
  render() {
    return (
      <View>
        <View style={style.container}>
          <View style={style.dispView}>
            <Button title="-" onPress={this.minsincr} />
          </View>
          <View style={style.dispText}>
            <Text>{this.props.count}</Text>
          </View>
          <View style={style.dispView}>
            <Button title="+" onPress={this.addincr} />
          </View>
        </View>
        <View style={[style.container, style.topMargin]}>
          <View style={style.dispText}>
            <Text>User Name:</Text>
          </View>
          <View style={style.dispView}>
            <Text>{this.props.user.name}</Text>
          </View>
        </View>
        <View style={[style.container, style.topMargin]}>
          <View style={style.dispText}>
            <TextInput
              placeholder="Enter new user name"
              onChangeText={this.addUserName}
            />
          </View>
        </View>
      </View>
    );
  }
}




const mapStateToProps = (state)=>{
   return {count: state.countState.count, user: state.userState};
}
const mapDispatchToprops = (dispatch)=>{
    return {
        addCount :(n) => dispatch({type:"ADD",payload:n}),
        minusCount :(n) => dispatch({type:"MINUS",payload:n}),
        addUserName :(n) => dispatch({type:"ADDUSER",payload:n})
    }
}
export default connect(mapStateToProps, mapDispatchToprops)(ReduxCount);

const style = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  topMargin:{
    marginTop : 20
  },
  dispText: {
    flex: 1,
    alignItems: 'center',
    justifyContent:"center"
  },
  dispView: {
    flex: 1,
  },
});