import React from 'react';
import {View, Text, Button,StyleSheet} from 'react-native';

export default props => {
  const {id, name, email, address, phone} = props.route.params;

  return (
    <View>
      <View style={styles.urow}>
        <View style={{flex: 1}}>
          <Text>Name</Text>
        </View>
        <View style={{flex: 2}}>
          <Text>{name}</Text>
        </View>
      </View>
      <View style={styles.urow}>
        <View style={{flex: 1}}>
          <Text>Email</Text>
        </View>
        <View style={{flex: 2}}>
          <Text>{email}</Text>
        </View>
      </View>
      <View style={styles.urow}>
        <View style={{flex: 1, fontSize: 50}}>
          <Text>address</Text>
        </View>
        <View style={{flex: 2}}>
          <Text>
            {address.street +
              ',' +
              address.suite +
              ',' +
              address.city +
              ',' +
              address.zipcode}
          </Text>
        </View>
      </View>
      <View style={styles.urow}>
        <View style={{flex: 1}}>
          <Text>Phone No</Text>
        </View>
        <View style={{flex: 2}}>
          <Text>{phone}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  urow: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
    padding: 10,
    color:"red"
  },
});