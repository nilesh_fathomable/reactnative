import React, {Component} from 'react';
import {View, Text, ActivityIndicator, FlatList, Button} from 'react-native';
import axios from 'axios';
import UserData from './UserData';
export default class FlatLists extends Component {
  constructor() {
    super();
    this.state = {
      data: [],
      loading:true
    };
  }
  async componentDidMount() {
    this.getData();
  }
  getData = async () => {
    try {
      let res = await axios.get('https://jsonplaceholder.typicode.com/users');
      // let res = await fetch('https://reactnative.dev/movies.json');
      this.setState({data: res.data,loading:false});
    } catch (error) {
      console.log('error', error);
      this.setState({loading:false});
    }
  };
  render() {
    if (this.state.loading) {
      return (
        <View style={{flex: 1,justifyContent:"center"}}>
          <ActivityIndicator size = "large"/>
        </View>
      );
    }
    return (
      <View style={{flex: 1}}>
        <Text>flatList</Text>
        <FlatList
          data={this.state.data}
          renderItem={({item}) => (
            <UserData
              style={{backgroundColor: '#ccc', height: 10}}
              userData={item}
              navigation = {this.props.navigation}
            />
          )}

          keyExtractor ={ (item)=> "thisisid"+item.id}
        />
      </View>
    );
  }
}
